
const execs = require('./execs')
let command = process.argv[2]
let supportedCommands = Object.getOwnPropertyNames(execs)
if(!~supportedCommands.indexOf(command)){
   notify(supportedCommands)
   process.exit(1)
}
execs[command]().then((msg)=>{
    console.log(msg)
    process.exit(0)
}).catch((err)=>{
    console.log(err)

})
function notify(commands){
    console.log('supported commands are:',JSON.stringify(commands))
}