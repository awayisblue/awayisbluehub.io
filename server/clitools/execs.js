const fs = require('fs')
const cheerio = require('cheerio')
module.exports.clean = async function(){
    let htmlPath = '../../pages/',
        markdownPath = '../../markdowns/',
        imagePath = '../static/img/',
        indexFilePath = '../../index.html'
    //清除生成的html文件
    let htmlFiles = fs.readdirSync(htmlPath)
    htmlFiles.forEach(function(filename,idx){
        if(/[^\.]+\..+$/.test(filename)){
            let unlinkPath = htmlPath+filename
            console.log('unlink:',unlinkPath)
            fs.unlinkSync(unlinkPath)
        }
    })
    //清除生成的markdown文件
    let markdownFiles = fs.readdirSync(markdownPath)
    markdownFiles.forEach(function(filename,idx){
        if(/[^\.]+\..+$/.test(filename)){
            let unlinkPath = markdownPath+filename
            console.log('unlink:',unlinkPath)
            fs.unlinkSync(unlinkPath)
        }
    })
    //清除上传的图片文件
    let imageFiles = fs.readdirSync(imagePath)
    imageFiles.forEach(function(filename,idx){
        if(/[^\.]+\..+$/.test(filename)){
            let unlinkPath = imagePath+filename
            console.log('unlink:',unlinkPath)
            fs.unlinkSync(unlinkPath)
        }
    })
    //清除生成的目录
    let indexHtml = fs.readFileSync(indexFilePath)
    let $ = cheerio.load(indexHtml,{decodeEntities:false})
    $('#post-list').empty()
    fs.writeFileSync(indexFilePath,getPageHtml($))
    return 'done'
}
function getPageHtml($){
    return "<html>" + $("html").html() + "</html>"
}
