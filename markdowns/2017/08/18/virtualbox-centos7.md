[TOC]

## 为什么这么做
平时使用的操作系统是windows，偶尔需要使用到linux操作系统，比如说学习，或者使用一些只能在linux上运行的软件。这个时候，你有很多选择，比如说：
1. 安装windows+linux双系统
2. 搭建vps，并使用远程登录的方式来使用linux系统
3. 使用虚拟机（vmware,virtualbox等）安装linux系统

对于1来讲，使用同一台电脑时，无法同时使用windows及linux，当你想要用linux来运行程序，windows来查资料时，就比较麻烦了。对于2来讲，除非你的软件需要发布到线上，否则这种方式也不方便，比如说安装的软件会影响线上，如果你只是为了测试或者练习的话，不建议使用线上的服务器来练手。第3种，就是我比较推荐的方式了，使用虚拟机的方式，可以让我们在本地运行多个操作系统，以现在的电脑配置，满足基本的需求是足够的了。

下面我介绍一下使用virtualbox安装centos操作系统的方式。

## 使用virtualbox安装centos7并实现联网
我的本地操作系统是win7-64位, 下面是实现的步骤：
1. 到virtualbox[下载页面](https://www.virtualbox.org/wiki/Downloads"下载页面")下载virtualbox,我下载的版本是[5.1.22](http://download.virtualbox.org/virtualbox/5.1.24/VirtualBox-5.1.24-117012-Win.exe)
2. 到centos7[下载页面](https://www.centos.org/download/ "下载页面")下载centos-minimal，我下载的是[CentOS-7-x86_64-Minimal-1611.iso](http://isoredirect.centos.org/centos/7/isos/x86_64/CentOS-7-x86_64-Minimal-1611.iso)
3. 使用virtualbox创建一个虚拟机并安装centos。
4. 在centos下，使用nmtui指令打开网络管理的gui，并activate connection
5. 在virtualbox里，网络设置成桥接模式，并选择当前联网的网卡，注意接入网线要勾上

经过以上步骤之后，就在virtualbox里安装了centos7，并且可以在centos7里面实现网络链接。而且由于是桥接模式，主机跟虚拟机之前可以直接通过ip地址进行局域网通讯。

## 一些简要的配置
这里记录一些简单的配置，后面有新的想法会持续更新。
### centos7使用epel库
默认情况下，yum所使用的库里的软件比较少，需要开启epel. 这一步是比较简单，但又比较重要的。
使用 `yum install -y epel-release`，安装epel-release后，就开启了。
使用`yum repolist`可以查看已有的仓库。

### 改变linux命令行文件夹的颜色

在centos下，使用ls指令，默认情况下，文件夹显示为蓝色，而背景色是黑色，蓝色跟黑色没什么对比度，很难看出来具体的文字是什么。这里做一下修改。
`vi ~/.bashrc`打开.bashrc文件，并在末尾加上`LS_COLORS=$LS_COLORS:'di=0;35:' ; export LS_COLORS`,保存文件，并运行`. ~/.bashrc`让改动生效。参于代码代表的意思，可以参考 https://askubuntu.com/questions/466198/how-do-i-change-the-color-for-directories-with-ls-in-the-console

## 问题解决

这里记录一些virtualbox或centos7使用过程中的问题及解决方式，后面有新的内容会持续更新。

### 解决外部主机无法访问virtualbox里的centos服务器的问题

在新装的centos里，在某一个端口下开了一个服务程序，但在win7里无法访问。virtualbox层面，我们设置了桥接模式，按道理是可以直接访问的，问题出现在centos的防火墙上。centos7防火墙使用了`firewalld`来管理。（关于firewalld的简要教程可以参考[how-to-set-up-a-firewall-using-firewalld-on-centos-7](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-using-firewalld-on-centos-7)）
我们使用`firewall-cmd`来操作防火墙，firewall-cmd有zone的概念，因为只是学习及练习目的，且系统是搭建在本地的，我们使用权限最大的zone（trusted）,使用下面的指令可以达到目的（注：我使用的是root用户）：
1. firewall-cmd --set-default-zone=trusted
2. systemctl restart network.service
3. systemctl restart firewalld.service

2及3的作用是使配置立即生效。


