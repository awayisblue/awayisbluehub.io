const render = require('./lib/render')
const fs = require('fs')
const uuid = require('uuid')
const cheerio = require('cheerio')
const moment = require('moment')
const mkdirp = require('mkdirp')
module.exports.index = async function(ctx){
	ctx.body = await render('index.ejs')
}
module.exports.create = async function(ctx){
    ctx.body = await render('create.ejs',{id:uuid.v4()})
}
module.exports.upload = async function(ctx){
    ctx.body = await render('upload.ejs')
}
module.exports.update = async function(ctx){
    let id = ctx.query.id,
        indexHtml = fs.readFileSync(getCategoryPath()),
        $ = cheerio.load(indexHtml,{decodeEntities:false}),
        $item = $('#post-list').find('.post-preview[data-id='+id+']')
    if($item.length===0){
        return ctx.throw(400,'博客不存在')
    }
    let title = $item.find('.post-title').text().trim(),
        date = $item.find('.post-meta').text(),
        href = $item.find('a').attr('href'),
        match = href.match(/([^\/]+)\.html/),
        slug = match[1]

    try{
        let markdown = fs.readFileSync(getMarkDownDirByDate(date)+slug+'.md')
        ctx.body = await render('update.ejs',{id,markdown,title,slug})
    }catch (err){
        ctx.throw(400,err)
    }
}
module.exports.delete = async function(ctx){
    let id = ctx.query.id,
        indexHtml = fs.readFileSync(getCategoryPath()),
        $ = cheerio.load(indexHtml,{decodeEntities:false}),
        $item = $('#post-list').find('.post-preview[data-id='+id+']')
    if($item.length===0){
        return ctx.throw(400,'博客不存在')
    }
    let date = $item.find('.post-meta').text(),
        href = $item.find('a').attr('href'),
        match = href.match(/([^\/]+)\.html/),
        slug = match[1]
    $item.next('hr').remove()
    $item.remove()
    try{
        fs.unlinkSync(getMarkDownDirByDate(date)+slug+'.md')
        fs.unlinkSync(getPageDirByDate(date)+slug+'.html')
        fs.writeFileSync(getCategoryPath(),getPageHtml($))
        ctx.body = 'delete ok'
    }catch (err){
        ctx.throw(400,err)
    }

}
module.exports.save = async function(ctx){
    let forms = ctx.request.body,
        {id,html,markdown,title,slug} = forms

    if(!(id&&html&&markdown&&title&&slug))return ctx.throw('参数出错')

    let blogTpl = fs.readFileSync(getTemplatePathByName('blog')),
        $ = cheerio.load(blogTpl,{decodeEntities:false})

    $('.editormd-preview-container').html(html)
    $('.post-heading h1').text(title)
    $('title').text(title)
    //图片在写的时候与在查看的时候路径有所变化，这里要做一下转换
    let $imgs = $('img')
    $imgs.each(function(){
        let $img = $(this),
            src = $img.attr('src'),
            //为了能在本地直接打开html正常查看,使用相对地址
            newSrc = src.replace(/^static\/img\//,'../../../../server/static/img/')

        $img.attr('src',newSrc)
    })


    let indexHtml = fs.readFileSync(getCategoryPath()),
        $c = cheerio.load(indexHtml,{decodeEntities:false}),
        $findExists = $c('#post-list .post-preview[data-id='+id+']'),
        date

    if($findExists.length===0){ //不存在，说明是新建的
        date = moment().format('YYYY-MM-DD')
        let $tpl = $c('<div/>').html($c('#tpl-post').html()),
            $item = $tpl.find('.post-preview'),
            dateSplit = date.split('-'),
            relativePath = './pages/'+dateSplit[0]+'/'+dateSplit[1]+'/'+dateSplit[2]+'/'+slug+'.html'
        $item.find('a').attr('href',relativePath)
        $item.find('.post-title').text(title)
        $item.attr('data-id',id)

        $item.find('.post-meta').text(date)
        $c('#post-list').prepend($tpl.html())
        //填入博客时间
        $('.intro-header .meta').text(date)
    }else{
        let $item = $findExists.first(),
            href = $item.find('a').attr('href'),
            arr = href.split('?'),
            originHref = arr[0],
            nowTime = (new Date).getTime()
        $item.find('a').attr('href',originHref+'?time='+nowTime)
        $item.find('.post-title').text(title)
        date = $item.find('.post-meta').text().trim()
        $('.intro-header .meta').text(date)
    }

    let markdownDir = getMarkDownDirByDate(date)
    await new Promise((resolve,reject)=>{
        mkdirp(markdownDir,err=>{
            if(err)return reject(err)
            resolve()
        })
    })

    let pageDir = getPageDirByDate(date)
    await new Promise((resolve,reject)=>{
        mkdirp(pageDir,err=>{
            if(err)return reject(err)
            resolve()
        })
    })
    fs.writeFileSync(markdownDir+slug.trim()+'.md',markdown)
    fs.writeFileSync(pageDir+slug.trim()+'.html',getPageHtml($))
    fs.writeFileSync(getCategoryPath(),getPageHtml($c))
    ctx.body = forms
}
module.exports.uploadImage = async function(ctx){
    let requestBody = ctx.request.body
    let {files} = requestBody
    let fileInfo = files.selectedFile

    let {size,type} = fileInfo

    if(size>1024*1024*2)ctx.throw(400,'文件大小不能超过2M')
    if(!~type.indexOf('image'))ctx.throw(400,'请上传图片类型的文件')
    ctx.body = fileInfo
}
module.exports.test = async function(ctx){
    console.log('test')
    ctx.set('Content-Type','text/html; charset=utf-8')
    ctx.body = fs.readFileSync('../index.html')
}

function getMarkDownDirByDate(date){
    let arr = date.split('-')
    return '../markdowns/' +arr[0]+'/'+arr[1]+'/'+arr[2]+'/'
}
function getPageDirByDate(date){
    let arr = date.split('-')
    return '../pages/' +arr[0]+'/'+arr[1]+'/'+arr[2]+'/'
}

function getPageHtml($){
    return "<html>" + $("html").html()+ "</html>"
}

function getCategoryPath(){
    return '../index.html'
}

function getTemplatePathByName(name){
    return './templates/'+name+'.html'
}

