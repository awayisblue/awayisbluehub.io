[TOC]

## 是否使用react
是否使用react的问题，可以理解成是是否使用virtual dom的问题。传统的项目一般是直接操作dom的，而react在dom上又抽象了一层virtual dom，使用react时，我们直接操作virtual dom,然后react再自动把改变更新到dom中。virtual dom的封装，使我们写代码的方式也发生了改变。
#### 代码的区别
写传统前端代码时，通常是一个个的eventListener来监听某些事件的触发,然后在这些listener的回调函数里处理业务逻辑，比如说请求api，操作UI（dom）等，当业务场景比较复杂时，代码的复杂度也会迅速上升，除非有良好的设计，不然实现起来是比较难的。

而react把UI及数据分离，写程序时，写好UI（virtual dom）并处理好UI根据state变化的逻辑，然后UI监听state的变化，当state变化时，UI自动进行重新渲染。写好UI后，我们可以专注于数据的变化，当业务逻辑变复杂时，代码的复杂度上升得也不快。
#### react的缺点
react有它的优点，也有它的缺点，主要有几个：
1. 生态没有jQuery那么完善，很多常用的前端工具在react里没有对应的实现 。
2. react处理transition比较麻烦，由于react是使用virtual DOM的，UI的变化反应在virtual DOM对真实DOM的操作上就类似于使用jquery进行简单粗暴的append或remove，transition在这上面是不起作用的。显然要做UI效果比较好看的页面，使用原生js是会更方便一些的。react处理transition使用了[TransitionGroup](https://reactcommunity.org/react-transition-group/ "TransitionGroup")来做，如果不嫌麻烦，也可以尝试一下。
3. react首屏渲染需要时间，一般react是把UI写在jsx代码里的，页面使用js动态生成，而使用原生的方式一般把UI写在html当中，这样，首屏加载时，两者就有区别：react的首屏加载时间上是要落后的。所以react会有短暂的首屏空白。

#### 遵循的原则
对于是否使用react的问题，如果对于首屏加载没有特殊要求，我主要遵循的原则如下：
1. 业务逻辑复杂而轻UI的，使用react
2. UI效果要求比较高而业务逻辑较为简单的，不使用react
3. 业务逻辑复杂，而UI要求较高的，需要做一个权衡，看两者占比怎样，做好选择。一般推荐react,因为TransitionGroup说麻烦，真要做也并不是太麻烦（权衡之下），而业务逻辑一团乱麻才是最糟的。
4. 业务需要使用较多前端UI通用组件，而团队又没有时间或实力自己开发的，不使用react。比如说后台管理系统，需要使用到数据可视化（[echarts](http://echarts.baidu.com/examples.html "echarts")）,通用表格组件（[bootstrap-table](http://bootstrap-table.wenzhixin.net.cn/ "bootstrap-table")）等前端工具，react没有对应的实现，自己做又不太可能。

#### react与jquery是否可以混用
我们知道react操作virtual dom, jquery操作dom,那么项目是否可以两者一起使用, 同时有两者的优点呢？通常是不这么做的，因为这两个工具在一定程度上是互斥的。当state改变时，react通过virtual DOM**自动**修改真实DOM,注意“自动”两个字，即是说，当react检测到state的变化而自动修改DOM时，你使用jquery所做的操作会被覆盖。所以说react跟jQuery一般是不一起使用的。

## 使用redux写react项目
纯react适合偏小型的项目，如果项目变得越来越大，需要管理的state越来越多时，使用redux会是一个更好的做法。[redux](http://redux.js.org/ "redux")把state统一到一个地方（store），主要通过以下的方法跟react交互起来：
1. 程序定义好数据的具体操作方式（**reducer**）
2. 程序定义好每个reducer所对应的**action**类型（供**dispatch**调用，调用时redux会运行对应的reducer来改变store里的state）
3. 让react UI监听store里某些**state**的变化，这些state变化时，UI会重新render

redux规范了你写react项目的方式，也可以让复杂项目的业务逻辑更加有条理，使扩展项目的功能变得更加容易。当然，对于简单的项目来讲，定义reducer及action类型是偏麻烦了一点，所以有的人主张不使用redux。
#### store是需要设计的
实际上，redux的好处这么大，但对开发者的要求也提高了不少，主要体现在**store是需要设计的**这一点上。store的糟糕设计会使项目代码变得很混乱，功能扩展也变得很困难。
如果你有数据库表（如MySQL）的设计经验，可能你更能够理解store设计得不好会让程序变得很复杂的这种说法。store设计不合理，可以类比成MySQL数据表设计不合理，数据库中，一个好的设计应该能让你用更好的效率，及更少的查询语句把你业务需要的数据查询出来。而不好的设计，可能需要你写n多个查询，各种联表操作，及后端代码的各种重新修饰处理，最后才把数据返回给前端，这当然增大了本不该有的代码，也提高了bug出现的概率。
store设计的目标很简单，那就是：**在满足现有业务下，让代码更简单，使扩展性更强**。具体表现在**store的扁平化设计**上。比如说下面两个store：
```js
{
   users:[
	   {
		  id:1,
		  name:'tom',
		  gender:'male',
		  contacts:[
			{
			  id:2,
			  name:'mike',
			  gender:'male',
			},
			{
			  id:3,
			  name:'jane',
			  gender:'female',
			}
		  ]
	   },
	   {
		  id:2,
		  name:'mike',
		  gender:'male',
		  contacts:[
			{
			  id:1,
			  name:'tom',
			  gender:'male',
			},
			{
			  id:3,
			  name:'jane',
			  gender:'female',
			}
		  ]
	   },
	   
   ]

}
```
```js
{
   users:[1,2],
   userInfos:{
     '1':{
	 	name:'tom',
	 	gender:'male',
	 },
	 '2':{
		  name:'mike',
		  gender:'male',
	 },
	 '3':{
		  name:'jane',
		  gender:'female',
	 }
   },
   contacts:{
      '1':[2,3],
	  '2':[1,3]
   }
}
```
很明显第二个store要比第一个store的设计要好得多，第一个看上去很直观，也很结构化，但基于这个结构的代码（reducer,UI的render逻辑等）是很难写的，代码运行效率不高，可扩展性也不强。而第二种并不直观（所以说需要设计），但没有第一种出现的那些问题。第一种是很典型的嵌套式设计，而第二种则是扁平化设计，redux store应该遵循扁平化的设计原则。

#### 使用redux-shape来创建store
在redux的官方例子中，action跟reducer是分开定义的，而action跟reducer一般是一一对应的。如果项目变复杂了，比如说reducer有上百个,action也有上百个，把它们分开在不同的地方，首先分开创建比较麻烦，其次是修改时很难对应起来。
理想的情况应该是action跟reducer一起创建，修改时，只改变一方就可以，而action跟reducer的一一对应关系让这种想法的实现变得很简单。于是我写了一个小库[redux-shape](https://github.com/awayisblue/redux-shape "redux-shape")来做这件事情。
使用的方式如下：

```js
  import {createStore,combineReducers} from 'redux'
  import reduxShape from 'redux-shape'
  //创建了一个名为users的leaf(redux-shape中的概念)
  let users = {
	state:[],
	reducers:{
		add(state,action){
		  return [...action.userId]
		}
	}
  }
  let userInfos = {
	state:{},
	reducers:{
		add(state,action){
		  let copy = JSON.parse(JSON.stringify(state)) //简单的deep copy方法
		  copy[action.userId] = action.userInfo
		  return copy
		}
	}
  }

  let contacts = {
	state:{},
	reducers:{
		add(state,action){
		  let copy = JSON.parse(JSON.stringify(state))
		  copy[action.userId] = action.contacts
		  return copy
		}
	}
  }
  //定义一个store tree,最后我们得到的store跟store tree的定义是一致的
  let tree = {
	  users:()=>users,
	  userInfos:()=>userInfos,
	  contacts:()=>contacts,
  }
  let reducer = reduxShape(combineReducers,{shape:tree,delimiter:'.'})
  let store = createStore(reducer)
```
这样，我们要运行users里的add reducer时，我们只需要调用store.dispatch({type:"users.add",userId:1}),就可以把userId为1的user添加进users里了。注意，我们并没有定义action类型，实际上action是随着store的创建而确定的，对于每个reducer来讲，都有一个统一格式的action类型。
当我们需要往里面加新的页面时，我们创建好UI组件,然后创建好一个像上面users,contacts一样的leaf,再添加到tree里面就行了，这种方式保证了项目的横向扩展性，对于业务扩展是很重要的。
在实际应用中，你可以把创建的store export出去，这是一个单例，可以在其他的代码模块里import这个store,随时使用这个store里的dispatch及getState方法，这无疑扩宽了业务的可能性，让业务逻辑的集成变得更加方便。

#### 使用redux-saga来处理side effect
在具体业务中，我们需要请求服务器api数据，或是在某一个action被dispatch之后，需要另一个action也被dispatch,这些情况在[redux-saga](https://redux-saga.js.org/ "redux-saga")中被称为side effect。这类业务场景并不在redux的体系当中，需要开发者自己写到别的地方去，比较好的方式是使用react中middleware的功能，而redux-saga就是负责middleware这一块的。

#### 全局初始化
在很多业务场景下，我们需要有一些全局初始化的操作，比如说创建一条websocket，初始化store, 定义一些全局可用的函数或变量，或者自定义一些调试功能（如快捷键把store输出到浏览器console当中）。这些场景是全局的，跟某一个UI组件，某一个redux里的reducer处理函数无关，那么，我们要把它们写到哪里去呢？这些逻辑可以在store创建完之后处理，比如说：
```
...
let reducer = reduxShape(combineReducers,{shape:tree,delimiter:'.'})
let store = createStore(reducer)
//下面处理初始化操作
websocket.connect();
...
```

## 总结
上面我们讲到了redux, redux-shape, redux-saga及全局初始化的概念，还有store作为单例被用在不同地方的可能性。这些都是以尽可能适用更多业务场景为目的而引伸出来的，知道了这些，我们就可以抗住大部分的业务场景了。
我之前写过一个react的例子，里面用到了redux,redux-shape,redux-saga，这里贴一下：[redux-shape-example](https://github.com/awayisblue/redux-shape-example "redux-shape-example")。



