const router = require('koa-router')();
const controller = require('./controller')

function registerRoutes(router){
    router.get('/',controller.index)
    router.get('/create',controller.create)
    router.get('/upload',controller.upload)
    router.get('/update',controller.update)
    router.get('/delete',controller.delete)
    router.post('/save',controller.save)
    router.post('/upload',controller.uploadImage);

    router.get('/test',controller.test)

}


module.exports = function(app){
    registerRoutes(router)
    app.use(router.routes()).use(router.allowedMethods())
}