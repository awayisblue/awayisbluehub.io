## github自建博客系统
由于github个人主页只能让前端代码执行，要在上面写博客，需要在本地编辑好博客网页，再提交到github。这个博客系统提供是在个人电脑上创建及编辑博客的功能，让写博客成为一件简单方便的事情。

## 提供的功能

- 博客创建
- 博客修改
- 博客删除
- 图片上传

## 相关技术
- 服务端使用NodeJs(v7.7.3)+KOA
- 编辑器使用Editor.md
- 网页前端展示使用boostrap

## 如何使用
如果你想要使用这个博客系统，可以按以下步骤来完成：
1. 在自己电脑上安装NodeJs v7.7.0以上
2. `https://github.com/awayisblue/awayisblue.github.io.git` 把本仓库clone到本地
3. 进到到server文件夹，并运行`npm install`
4. 若你想清除已有的博客，可以进入到clitools文件夹，运行`node index clean`
5. 在server文件夹下， 运行`npm run start`
6. 在浏览器里打开`http://localhost:3000`,这样你就可以创建自己的博客了。
