[TOC]
## github博客的优缺点
我们知道github可以搭建自己的博客，在github上写博客的优点有很多，比如说：
 - 成本低廉，你并不需要花钱去买VPS，就可以建自己的博客系统。
 - 高可自定义性，如果你把博客写到CSDN或者博客园等第三方博客平台上，你就只能使用它提供的功能，无法自定义。
 - 内容安全性更高，内容放github上，应该没人担心某一天会全没了吧。

实际上，可以说在github上搭建博客，同时拥有第三方博客平台和传统自建博客平台的优点。但也有一个缺点，github只能让我们执行web前端代码，却无法提供服务端的功能。

## 博客搭建思路
那么，我们要如何创建及发布我们的博客呢？总不能手动把博客内容写到html里，然后提交到github上吧。解决的思路是：
1. 在自己的电脑上开服务器程序，访问本地链接，提供博客创建的相关功能
2. 博客保存时，以文件的形式保存起来
3. 把生成的文件commit并push到github，这样，你的博客就发布出去了。

## 实现方式
为了实现这个想法，我使用nodejs+koa写了服务端的程序，提供的功能有：
- 博客创建
- 博客修改
- 博客删除
- 图片上传

由于我比较喜欢使用**markdown**编辑器，编辑器使用了[Editor.md](https://pandao.github.io/editor.md/en.html "Editor.md")
博客的前端展示使用boostrap，并以[clean blog](https://startbootstrap.com/template-overviews/clean-blog/)作为原型进行修改，适配手机端及PC端。由于自己设计方面不擅长，所以UI并不是太好看，以内容展现为主。

## 实现结果
现在你所看到的博客就是我使用这个博客系统写的了。想要看代码可以看我的仓库[awayisblue.github.io](https://github.com/awayisblue/awayisblue.github.io "awayisblue.github.io")
