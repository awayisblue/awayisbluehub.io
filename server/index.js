const Koa = require('koa');
const serve = require('koa-static')
const json = require('koa-json')
const mount = require('koa-mount')
const app = new Koa()
const bodyParser = require('koa-body')
const staticMountPoint = new Koa()
require('koa-qs')(app, 'first')
app.use(json())
app.use(bodyParser({
    formidable:{
        uploadDir: './static/img/',
        keepExtensions:true,
        maxFieldsSize :2*1024*1024,//最大2M
    },
    multipart: true,
    urlencoded: true,
    jsonLimit:'20mb',
    formLimit:'20mb',
    textLimit:'20mb',
}))
//static files
staticMountPoint.use(serve('./static',{maxage:60 * 60 * 24 * 365}))
app.use(mount('/static',staticMountPoint))
require('./router')(app)
app.listen(3000);
console.log('http://localhost:3000')